import makeLog from './registerView.vue'
import { createApp } from 'vue'
import '@arco-design/web-vue/dist/arco.css';

const app = createApp(makeLog);
app.mount(`#register`);
