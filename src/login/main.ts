import makeLog from './loginView.vue'
import { createApp } from 'vue'
import '@arco-design/web-vue/dist/arco.css';
const app = createApp(makeLog);
app.mount(`#login`);
