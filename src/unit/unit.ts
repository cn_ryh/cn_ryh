
export const formatDateTime = function (date: Date) {
    let s = ``
    const y = date.getFullYear();
    s += y + '-';
    const m = date.getMonth() + 1;
    s += m < 10 ? ('0' + m) : m;
    s += '-'
    const d = date.getDate();
    s += d < 10 ? ('0' + d) : d;
    s += '  '
    const h = date.getHours();
    s += h < 10 ? ('0' + h) : h;
    s += ':'
    const minute = date.getMinutes();
    s += minute < 10 ? ('0' + minute) : minute;
    s += ':'
    const second = date.getSeconds();
    s += second < 10 ? ('0' + second) : second;
    return s;
}