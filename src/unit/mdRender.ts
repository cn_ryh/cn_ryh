import markdownit from 'markdown-it'
import mathjax from 'markdown-it-mathjax3'
import { alert as mdAlert } from '@mdit/plugin-alert'
import { container as mdContainer } from '@mdit/plugin-container'
import { footnote as mdFootnote } from '@mdit/plugin-footnote'
import { imgLazyload as mdLazyload } from '@mdit/plugin-img-lazyload'
import { tab as mdTab } from '@mdit/plugin-tab'
import { tasklist as mdTasklist } from '@mdit/plugin-tasklist'
import { mark as mdMark } from '@mdit/plugin-mark'
import hljs from 'highlight.js'
import 'highlight.js/styles/github.css';
import './markdown.css'
const md: markdownit = new markdownit({
    html: true,
    highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
            try {
                return '<pre class="hljs"><code>' +
                    hljs.highlight(lang, str, true).value +
                    '</code></pre>';
            } catch (err) {
                console.error(err);
            }
        }

        return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
    }
}).use(mathjax).use(mdAlert).use(mdContainer, { name: "warning" }).use(mdFootnote).use(mdLazyload).use(mdTab, { name: "tabs" }).use(mdTasklist).use(mdMark)

export default md