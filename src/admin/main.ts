import App from './loaderViews.vue'
import { createApp } from 'vue'
import { router } from './loader'
import ArcoVue from '@arco-design/web-vue';
import '@arco-design/web-vue/dist/arco.css';

const app = createApp(App);

app.use(ArcoVue);
app.use(router);
app.mount('#admin')
