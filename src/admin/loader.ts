// 1. 定义路由组件.
// 也可以从其他文件导入
import * as VueRouter from 'vue-router'
import editBlogView from './editBlog.vue'
import notFound from '../404Page.vue'
import newBlogView from './newBlog.vue'
import AV from 'leancloud-storage'
AV.init({
    appId: 'InIuiRboDcEwyjh8iYSIaD3N-gzGzoHsz',
    appKey: "NLQOSgDwS6aitCPstpgOCMaV",
    serverURL: "https://iniuirbo.lc-cn-n1-shared.com"
})
// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。

const routes = [
    { path: '/editblog/:id', component: editBlogView },
    { path: '/newblog', component: newBlogView },
    { path: '/:pathMatch(.*)*', name: 'not-found', component: notFound },
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = VueRouter.createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: VueRouter.createWebHashHistory(),
    routes, // `routes: routes` 的缩写
})
router.beforeEach(() => {
    return new Promise((resolve) => {
            const user = AV.User.current();
            console.log(AV.User.current());
            if (!user) {
                // window.alert(`您尚未登录`)
                resolve(true);
                return;
            }
            if (user.get("admin")) {
                resolve(true);
            }
            else {
                window.alert(`您没有权限访问该页面`)
                resolve(false);
            }

    })

})
export { router }
